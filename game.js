(function(){var gameport = document.getElementById("gameport");

var game_width = 912;
var game_length = 608;

var running = false;  // boolean flag for game running
var width = 608;
var length = 608;
var num_targets = 10;

var renderer = PIXI.autoDetectRenderer(width, length, {backgroundColor: 0x5b6ee1});
gameport.appendChild(renderer.view);

var entity_layer, tu, world;

var tut_text = new PIXI.Text("Use A and D keys to move the tank.\nUse the space bar to shoot.\nPick up powerups and \nkeep shooting the targets!\n\nPress ESC to return to the main menu.", {font: "18px Arial"});
tut_text.position.x = 175;
tut_text.position.y = 200;
var cred_text = new PIXI.Text("   Developed by Vince Messenger\n            Messenger Games\n\nPress ESC to return to the main menu.", {font: "18px Arial"});
cred_text.position.x = 175;
cred_text.position.y = 200;
var text = new PIXI.Text("GAME OVER\n\nPress enter to play again.\nPress ESC to return to the main menu.", {font: "18px Arial"});
text.position.x = 175;
text.position.y = 200;

var texture = PIXI.Texture.fromImage("tank.png");

var tank;
var orig_tank_speed = 3;
var tank_speed = orig_tank_speed;
var orig_shoot_speed = 500;         // rate of fire for tank in milliseconds
var shoot_speed = orig_shoot_speed;   
var last_shot;          // time of last fire

// vars for all the different game screens
var stage = new PIXI.Container();   // game screen
var start_screen = new PIXI.Container();   // start screen
var end_screen = new PIXI.Container();     // game over screen
var credit_screen = new PIXI.Container();  // credits screen
var tutorial_screen = new PIXI.Container();
var current_stage = start_screen;

// add background image
//var bg = new PIXI.Sprite(new PIXI.Texture.fromImage("background.png"));
var bg = new PIXI.Sprite(new PIXI.Texture.fromImage("background.png"));
var go_bg = new PIXI.Sprite(new PIXI.Texture.fromImage("background.png"));   // game_over background
var start_bg = new PIXI.Sprite(new PIXI.Texture.fromImage("background.png"));   // start_screen background
var tut_bg = new PIXI.Sprite(new PIXI.Texture.fromImage("background.png"));
var cred_bg = new PIXI.Sprite(new PIXI.Texture.fromImage("background.png"));

stage.addChild(bg);
end_screen.addChild(go_bg);
start_screen.addChild(start_bg);
tutorial_screen.addChild(tut_bg);
credit_screen.addChild(cred_bg);

// create menu sprite animation
PIXI.loader
  .add("assets.json")
  .load(ready);

function ready() {
	var frames = [];

	for (var i=1; i<=11; i++) {
	    frames.push(PIXI.Texture.fromFrame('tank_animation' + i + '.png'));
	}

  	runner = new PIXI.extras.MovieClip(frames);
	runner.position.x = 150;
	runner.position.y = 470;
	runner.animationSpeed = 0.1;
	runner.play();
	start_bg.addChild(runner);
}

// add text to screens
cred_bg.addChild(cred_text);
tutorial_screen.addChild(tut_text);

// powerup vars!
var spawned;
var last_powerup_spawn;   // time of last spawn
var cur_powerup;
var powerup_time;  // count down from 5 or so
var rapid_fire_sprite = new PIXI.Sprite(new PIXI.Texture.fromImage("rapid_fire.png"));
var scatter_shot_sprite = new PIXI.Sprite(new PIXI.Texture.fromImage("scatter_shot.png"));
var speed_boost_sprite = new PIXI.Sprite(new PIXI.Texture.fromImage("speed_boost.png"));
var god_mod_sprite = new PIXI.Sprite(new PIXI.Texture.fromImage("god_mode.png"));
var powerup_station = new PIXI.Sprite(new PIXI.Texture.fromImage("powerup_station.png"));
var scatter_shot;
var speed_boost;
var rapid_fire;
var god_mode;
var power_ups = ['rapid_fire', 'scatter_shot', 'speed_boost', 'god_mode'];   // array for randomly choosing a powerup to spawn

var bullets = [];   // array for holding active bullets
var targets = [];   // array for holding targets

var score;
var score_sprite;

// vars regarding the timer
var count;
var timer;
var time;

// vars for processInput()
var goRight, goLeft;

PIXI.loader
  .add('map_json', 'map.json')
  .add('tileset', 'map.png')
  .load();

 function call_main() {
 	main_menu();
 }

var menu, play, tutorial, credits;

// create start screen menu
menu = new PIXI.Container();
menu.position.x = 260;
menu.position.y = 220;
start_bg.addChild(menu);

// add play to menu
play = new PIXI.Text("Play", {font: "35px Arial"});
menu.addChild(play);
play.interactive = true;
play.mouseover = hover_start;
play.mouseout = hover_end;

// add tutorial to menu
tutorial = new PIXI.Text("Tutorial", {font: "35px Arial"});
menu.addChild(tutorial);
tutorial.interactive = true;
tutorial.mouseover = hover_start;
tutorial.mouseout = hover_end;
tutorial.position.y = 40;
tutorial.position.x = -22;

// add credits to menu
var credits = new PIXI.Text("Credits", {font: "35px Arial"});
menu.addChild(credits);
credits.interactive = true;
credits.mouseover = hover_start;
credits.mouseout = hover_end;
credits.position.y = 80;
credits.position.x = -17;

// tutorial click function
tutorial.click = function() {
	current_stage = tutorial_screen;
}

// credits click
credits.click = function() {
	current_stage = credit_screen;
}

// define hover functions
function hover_start() {
	this.scale.x = 1.25;
	this.scale.y = 1.25;
}

function hover_end() {
	this.scale.x = 1;
	this.scale.y = 1;
}

// play click function
play.click = function() {
	init();
}



function init() {
	current_stage = stage;

	tu = new TileUtilities(PIXI);
	world = tu.makeTiledWorld("map_json", "map.png");
    stage.addChild(world);
    entity_layer = world.getObject("GameObjects");

	// sprite init
	tank = new PIXI.Sprite(texture);
    tank.position.x = 200;
	tank.position.y = 470;
	tank.anchor.x = .5;
	
	// bullet init
	bullet = new PIXI.Sprite(PIXI.Texture.fromImage("bullet.png"));
	bullet.position.x = tank.position.x + 43;
	bullet.position.y = tank.position.y + 28;
	bullet.visible = false;
	
	// targets init and add to stage
	for (var i = 0; i < num_targets; i++) {
		targets[i] = new PIXI.Sprite(PIXI.Texture.fromImage("target.png"));
		targets[i].position.x = (Math.random() * ((game_width-70)-30)) + 30;
		targets[i].position.y = Math.random() * game_length/2;  // limit targets from going below half the screen
		var scale = Math.random() * 1.25 + .5;   // find a random scale for each target
		targets[i].scale.x = scale;
		targets[i].scale.y = scale;
		targets[i].radius = 23 * scale;  // set and scale radius
		entity_layer.addChild(targets[i]);
	}
	
	// powerup init
	rapid_fire = false;
	speed_boost = false;
	scatter_shot = false;
	god_mode = false;
  
  	// vars regarding the timer
	count = 90;  // intended 90 second game
	timer = new PIXI.Text("Time: " + count);
	timer.position.x = 100;
	timer.position.y = 3;
	stage.addChild(timer);
	
	// score init
	score = 0;
	score_sprite = new PIXI.Text("Score: " + score);
	score_sprite.position.x = 350;
	score_sprite.position.y = 3;
	stage.addChild(score_sprite);
	
	//powerup_station initd
	powerup_station.position.x = 800;
	powerup_station.position.y = 470;
	entity_layer.addChild(powerup_station);

    // add things to entity_layer
    entity_layer.addChild(tank);

	// spawn powerup
	spawnPowerup();

    world.position.x = 0;

	running = true;  // restart game

}

function spawnPowerup() {
	cur_powerup = power_ups[Math.floor(Math.random() * power_ups.length)]; // choose random powerup
	if (cur_powerup == 'god_mode') {
		cur_powerup = power_ups[Math.floor(Math.random() * power_ups.length)]; // choose again to make god_mode more rare
	}

	//cur_powerup = 'god_mode';  // hard code for testing

	if (cur_powerup == 'rapid_fire') {
		cur_powerup = rapid_fire_sprite;
	}

	if (cur_powerup == 'scatter_shot') {
		cur_powerup = scatter_shot_sprite;
	}

	if (cur_powerup == 'speed_boost') {
		cur_powerup= speed_boost_sprite;
	}

	if (cur_powerup == 'god_mode') {
		cur_powerup = god_mod_sprite;
	}

	cur_powerup.position.x = 850;
	cur_powerup.position.y = 520;
	cur_powerup.anchor.x = .5;
	entity_layer.addChild(cur_powerup);
	cur_powerup.visible = true;
	spawned = true;

}

// define keydownListener
function keydownListener(key) {

	if (key.keyCode == 65) {   // a key
		goLeft = true;
		//sprite.position.x -= 5;
	}

	if (key.keyCode == 68) {   // d key
		goRight = true;
		//sprite.position.x += 5;
	}

	if (key.keyCode == 13 && !running) {   // enter key
		init();   // restart game 
	}
	
	if (key.keyCode == 32 && running) {   // space key
		key.preventDefault();
		shoot();
	}

	if (key.keyCode == 27 && !running) {
		current_stage = start_screen;
	}

	if (key.keyCode == 13 && !running && current_stage == stage) {
		init();
	}
}

// define keyupListener
function keyupListener(key) {
		if (key.keyCode == 65) {   // a key
		goLeft = false;
	}

	if (key.keyCode == 68) {   // d key
		goRight = false;
	}

}

function shoot() {
	if (!last_shot || new Date().getTime() - last_shot >= shoot_speed) {
	  	last_shot = new Date().getTime();   // get time of shot
	    
	    if (scatter_shot) {
	    	for (var i = 0; i < 3; i++){
	    		bullets[bullets.length] = new PIXI.Sprite(PIXI.Texture.fromImage("bullet.png"));
				bullets[bullets.length-1].visible = true;  // must use bullets.length-1 after adding to array
				bullets[bullets.length-1].position.x = tank.position.x - 6;
				bullets[bullets.length-1].position.y = tank.position.y + 28;
				bullets[bullets.length-1].radius = 5;
				entity_layer.addChild(bullets[bullets.length-1]);
				createjs.Tween.get(bullets[bullets.length-1].position).to({x: bullets[bullets.length-1].position.x - 55 + i * 55 , y: -100}, 1000, createjs.Ease.linear)
	    	}
	    }
	    else {
			bullets[bullets.length] = new PIXI.Sprite(PIXI.Texture.fromImage("bullet.png"));
			bullets[bullets.length-1].visible = true;  // must use bullets.length-1 after adding to array
			bullets[bullets.length-1].position.x = tank.position.x - 6;
			bullets[bullets.length-1].position.y = tank.position.y + 28;
			bullets[bullets.length-1].radius = 5;
			entity_layer.addChild(bullets[bullets.length-1]);
			createjs.Tween.get(bullets[bullets.length-1].position).to({y: -100}, 1000, createjs.Ease.linear);
		}
	}
}

function circle_intersection(c0, c1) {
  // Use the radius to find the center of the circle:
  var c0x = c0.position.x + c0.radius;
  var c0y = c0.position.y + c0.radius;
  var c1x = c1.position.x + c1.radius;
  var c1y = c1.position.y + c1.radius;

  // Use the distance formula between (c0x, c0y) and (c1x, c1y):
  var cdx = c1x - c0x;
  var cdy = c1y - c0y;
  var d = c0.radius + c1.radius;
  var nd = (cdx * cdx) + (cdy * cdy);
  if (nd < d*d) {
    return true;
  }
  return false;
}

function checkCollisions() {
	  // Check intersection between bullets and targets:
    for (var i in bullets) {
    	for (var j in targets)
	      if (bullets[i].visible == true && circle_intersection(bullets [i], targets[j])) {
	        bullets[i].visible = false;
	        
	        // move target to another random position with new scale
	        targets[j].position.x = (Math.random() * ((game_width-30)-40)) + 40;
			targets[j].position.y = Math.random() * game_length/2;  // limit targets from going below half the screen
			var scale = Math.random() * 1.25 + .5;   // find a random scale for each target
			targets[j].scale.x = scale;
			targets[j].scale.y = scale;
			targets[j].radius = 23 * scale;  // set and scale radius
			
			// update score
			score++;
			score_sprite.text = "Score: " + score;
	      }
    }
    // Check intersection between tank and cur_powerup
    if (Math.abs(tank.position.x - cur_powerup.position.x) < 65 && cur_powerup.visible) {
    	if (cur_powerup == rapid_fire_sprite) {
    		rapid_fire = true;  // turn on rapid fire
    		shoot_speed = 0;
    	}

    	if (cur_powerup == scatter_shot_sprite) {
    		scatter_shot = true;
    	}

    	if (cur_powerup == speed_boost_sprite) {
    		speed_boost = true;
    		tank_speed = 5;
    	}

    	if (cur_powerup == god_mod_sprite) {
			rapid_fire = true;  // turn on rapid fire
			shoot_speed = 0;

			speed_boost = true; // turn on speed boost
			tank_speed = 5;

			scatter_shot = true; // turn on rapid_fire
    	}

    	powerup_time = new Date().getTime();
    	cur_powerup.visible = false;
    }
    	
}

function cleanUp() {   // cleanup bullets array
	for (var i in bullets) {
		if (bullets[i].position.y < 0)
			bullets.splice(i, 1); // remove bullet that has collided with top of screen
	}
}

function checkPowerUps() {

	if (new Date().getTime() - powerup_time > 5000) {
		if (rapid_fire) {
			rapid_fire = false;
			shoot_speed = orig_shoot_speed;
			spawned = false;   // set flag that says ready to spawn powerup
		}

		if (scatter_shot) {
			scatter_shot = false;
			spawned = false;
		}

		if (speed_boost) {
			speed_boost = false;
			tank_speed = orig_tank_speed;
			spawned = false;
		}

	}

	if (new Date().getTime() - powerup_time > 15000 && !spawned) {   // wait 15 seconds and span another powerup
		spawnPowerup();
	}
	
}

function moveCamera() {
	if (tank.position.x > 520){
		createjs.Tween.get(world.position).to({x: -300}, 400, createjs.Ease.linear);
	}
	if (tank.position.x < 400) {
		createjs.Tween.get(world.position).to({x: 0}, 400, createjs.Ease.linear);	
	}
}

function game() {
    if (running) {

	    if (!time){      // get start time
	    	time = new Date().getTime();
	    }

		processInput();
		moveCamera();
		checkCollisions();
		cleanUp();
		checkClock();
		checkPowerUps();
	}
}

function checkClock() {
	if (new Date().getTime() - time >= 1000){
		timer.text = "Time: " + count;
		time = new Date().getTime();

		if (count == 0) {
			running = false;
			stage.addChild(text);
		}

		count--; // increment counter
	}

}

function processInput() {
	if (goLeft && tank.position.x > 42) {
			tank.position.x -= tank_speed;
	}

	if (goRight && tank.position.x < game_width-39) {
		tank.position.x += tank_speed;
	}

}

// add event listeners
document.addEventListener('keydown', keydownListener);
document.addEventListener('keyup', keyupListener);
function animate() {
requestAnimationFrame(animate);
renderer.render(current_stage);
}

function update() {
	setInterval(game, 15);
}

//init();
animate();
update();
})();
